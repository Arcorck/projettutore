$(function() {

    $("#buttonProduits").click(getProduits);
    
    
    //liste tous les users de la base
    async function getProduits(){
        let rep = await fetch('http://localhost:8000/ecommerce/api/v1.0/produit', { method: 'GET' });
        let reponse = await rep.json();
        console.log(reponse);
        $('#enteteProduit').empty();
        $('#enteteProduit').append($('<th>').text("Nom"));
        $('#enteteProduit').append($('<th>').text("Ref"));
        $('#enteteProduit').append($('<th>').text("Prix"));
        $('#containerProduit').empty();
        for (let i=0;i<reponse.produit.length;++i){
          $('#containerProduit').append($('<tr id='+i+' class="table-success">'));
          $('#'+i).append($('<td>').text(reponse.produit[i].nom));
          $('#'+i).append($('<td>').text(reponse.produit[i].ref));
          $('#'+i).append($('<td>').text(reponse.produit[i].prix));
          $('#'+i).append($('<input id="buttonSupProduits" type="button" value="Supprimer" />'));
        }
    }

    $("#buttonSupProduits").click(SupprimerProduit);
});