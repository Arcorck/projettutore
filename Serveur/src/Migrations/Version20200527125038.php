<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527125038 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, mdp VARCHAR(255) NOT NULL)');
        $this->addSql('DROP INDEX IDX_29A5EC27BCF5E72D');
        $this->addSql('DROP INDEX IDX_29A5EC274827B9B2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, categorie_id INTEGER DEFAULT NULL, marque_id INTEGER DEFAULT NULL, nom_produit VARCHAR(255) NOT NULL COLLATE BINARY, ref_produit BIGINT NOT NULL, prix DOUBLE PRECISION NOT NULL, CONSTRAINT FK_29A5EC27BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_29A5EC274827B9B2 FOREIGN KEY (marque_id) REFERENCES marque (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO produit (id, categorie_id, marque_id, nom_produit, ref_produit, prix) SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
        $this->addSql('CREATE INDEX IDX_29A5EC27BCF5E72D ON produit (categorie_id)');
        $this->addSql('CREATE INDEX IDX_29A5EC274827B9B2 ON produit (marque_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE user');
        $this->addSql('DROP INDEX IDX_29A5EC27BCF5E72D');
        $this->addSql('DROP INDEX IDX_29A5EC274827B9B2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, categorie_id INTEGER DEFAULT NULL, nom_produit VARCHAR(255) NOT NULL, ref_produit BIGINT NOT NULL, prix DOUBLE PRECISION NOT NULL, marque_id INTEGER NOT NULL)');
        $this->addSql('INSERT INTO produit (id, categorie_id, marque_id, nom_produit, ref_produit, prix) SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
        $this->addSql('CREATE INDEX IDX_29A5EC27BCF5E72D ON produit (categorie_id)');
        $this->addSql('CREATE INDEX IDX_29A5EC274827B9B2 ON produit (marque_id)');
    }
}
