<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200602102701 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE panier (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER DEFAULT NULL, somme_total DOUBLE PRECISION NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_24CC0DF2A76ED395 ON panier (user_id)');
        $this->addSql('CREATE TABLE panier_produit (panier_id INTEGER NOT NULL, produit_id INTEGER NOT NULL, PRIMARY KEY(panier_id, produit_id))');
        $this->addSql('CREATE INDEX IDX_D31F28A6F77D927C ON panier_produit (panier_id)');
        $this->addSql('CREATE INDEX IDX_D31F28A6F347EFB ON panier_produit (produit_id)');
        $this->addSql('DROP INDEX IDX_29A5EC27BCF5E72D');
        $this->addSql('DROP INDEX IDX_29A5EC274827B9B2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, categorie_id INTEGER DEFAULT NULL, marque_id INTEGER DEFAULT NULL, nom_produit VARCHAR(255) NOT NULL COLLATE BINARY, ref_produit BIGINT NOT NULL, prix DOUBLE PRECISION NOT NULL, CONSTRAINT FK_29A5EC27BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_29A5EC274827B9B2 FOREIGN KEY (marque_id) REFERENCES marque (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO produit (id, categorie_id, marque_id, nom_produit, ref_produit, prix) SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
        $this->addSql('CREATE INDEX IDX_29A5EC27BCF5E72D ON produit (categorie_id)');
        $this->addSql('CREATE INDEX IDX_29A5EC274827B9B2 ON produit (marque_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, nom, prenom, email, mdp FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, panier_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL COLLATE BINARY, prenom VARCHAR(255) NOT NULL COLLATE BINARY, email VARCHAR(255) NOT NULL COLLATE BINARY, mdp VARCHAR(255) NOT NULL COLLATE BINARY, telephone VARCHAR(255) NOT NULL, CONSTRAINT FK_8D93D649F77D927C FOREIGN KEY (panier_id) REFERENCES panier (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO user (id, nom, prenom, email, mdp) SELECT id, nom, prenom, email, mdp FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F77D927C ON user (panier_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE panier');
        $this->addSql('DROP TABLE panier_produit');
        $this->addSql('DROP INDEX IDX_29A5EC27BCF5E72D');
        $this->addSql('DROP INDEX IDX_29A5EC274827B9B2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, categorie_id INTEGER DEFAULT NULL, marque_id INTEGER DEFAULT NULL, nom_produit VARCHAR(255) NOT NULL, ref_produit BIGINT NOT NULL, prix DOUBLE PRECISION NOT NULL)');
        $this->addSql('INSERT INTO produit (id, categorie_id, marque_id, nom_produit, ref_produit, prix) SELECT id, categorie_id, marque_id, nom_produit, ref_produit, prix FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
        $this->addSql('CREATE INDEX IDX_29A5EC27BCF5E72D ON produit (categorie_id)');
        $this->addSql('CREATE INDEX IDX_29A5EC274827B9B2 ON produit (marque_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649F77D927C');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, nom, prenom, email, mdp FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, mdp VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO user (id, nom, prenom, email, mdp) SELECT id, nom, prenom, email, mdp FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
    }
}
