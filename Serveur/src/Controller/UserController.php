<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;

/**
* @Route("ecommerce/api/v1.0", name="produit")
*/
class UserController extends AbstractController
{
    /**
    * Permet d'avoir la liste de tous les Utilisateurs
    * @Route("/user", name="liste_user", methods={"GET"})
    */
    public function listeUser()
    {
        $repository   = $this->getDoctrine()->getRepository(User::class);
        $listeUser  = $repository->findAll();
        $listeReponse = array();
        foreach ($listeUser as $user) {
                $listeReponse[] = array(
                'id'     => $user->getId(),
                'nom'    => $user->getNom(),
                'prenom'    => $user->getPrenom(),
                'email'   => $user->getEmail(),
                'telephone' =>$user->getTelephone(),
                'mdp'   => $user->getMdp(),
                'panier'    =>$user->getPanier()
            );
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("user"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet d'avoir un utilisateur grâce à son id
     * @Route("/user/{id}", name="details_user", methods={"GET"})
     */
    public function detailsUser($id){
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->find($id);
        $panier = $user->getPanier();
        $reponse = new Response(json_encode(array(
                        'id'     => $user->getId(),
                        'nom'    => $user->getNom(),
                        'prenom'    => $user->getPrenom(),
                        'email'   => $user->getEmail(),
                        //'id_panier' => $panier->getId()
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet de créer un utilisateur
     * @Route("/user", name="nouveau_user", methods={"POST"})
     */
    public function nouveauUSer(Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $user = new User();
        $body   = json_decode($request->getContent(), true);
        $nom    = $body['nom'];
        $user->setNom($nom);
        $prenom    = $body['prenom'];
        $user->setPrenom($prenom);
        $email   = $body['email'];
        $user->setEmail($email);
        $telephone   = $body['telephone'];
        $user->setTelephone($telephone);
        $mdp   = $body['mdp'];
        $user->setMdp($mdp);
        $entityManager->persist($user);
        $entityManager->flush();
        $reponse = new Response(json_encode(array(
                        'id'     => $user->getId(),
                        'nom'    => $user->getNom(),
                        'prenom'    =>$user->getPrenom(),
                        'id_panier' =>$user->getPanier(),
                        )
                ));
        //$panier = new Panier();
        //$entityManager->persist($panier);
        //$entityManager->flush();

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }


    /**
     * Permet de supprimer un user grâce à son id
     * @Route("/user/{id}", name="suppression_user", methods={"DELETE"})
     */
    public function suppressionUser(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(User::class);
        $body          = json_decode($request->getContent(), true);
        $user        = $repository->find($id);
        $entityManager->remove($user);
                $entityManager->flush();
                $reponse = new Response(json_encode(array(
                        'suppression'    => "supprimé",
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "");
        return $reponse;
    }


   /**
     * Permet de modifier le user grâce à son id
     * @Route("/user/{id}", name="modification_user", methods={"PUT"})
     */
    public function modificationUser(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(User::class);
        $body          = json_decode($request->getContent(), true);
        $user        = $repository->find($id);
        if (isset($body['nom'])){
            $nom = $body['nom'];
            $user->setNom($nom);
        }
        if (isset($body['prenom'])){
            $prenom = $body['prenom'];
            $user->setPrenom($prenom);
        }
        if (isset($body['email'])){
            $email = $body['email'];
            $user->setEmail($email);
        }
        if (isset($body['telephone'])){
            $telephone = $body['telephone'];
            $user->setTelephone($telephone);
        }
        if(isset($mdp)){
            $mdp = $body['mdp'];
            $user->setMdp($mdp);
        }
        $entityManager->persist($user);
                $entityManager->flush();
                $reponse = new Response(json_encode(array(
                        'id'     => $user->getId(),
                        'nom'    => $user->getNom(),
                        'prenom'    =>$user->getPrenom(),
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}